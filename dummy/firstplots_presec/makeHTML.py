#!/env/python
import glob,os
from html import HTML

#SETUP
studyName = "firstplots_presec"
email = "steffen.henkelmann@cern.ch"


h = HTML()
def printHeader():
    study = open(studyName + '.html', 'w')
    print >> study.write("""<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>UBC top particle physics | Portfolio :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Your Trip Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="../js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<!-- pop-up-script -->
<script src="../js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="../css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.img-top a').Chocolat();
		});
		</script>
<!-- //pop-up-script -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
</head>
    """)
    study.close()


def printBanner():
    with open(studyName+'.html', "a") as f:
        f.write("""<body>
<!-- banner -->
<div class="banner">
<div class="container">
<div class="banner_top">
<div class="banner_top_left">
  <p>The top particle physics
  team @UBC</p>
</div>
<div class="banner_top_right">
  <form>
  <input type="text" value="Search Here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}" required="">
  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
  </form>
</div>
<div class="clearfix"> </div>
</div>
<nav class="navbar navbar-default">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
</div>

<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav cl-effect-14">
    <li><a href="../index.html"class="active">Alison</a></li>
    <li><a href="../matthias.html">Matthias</a></li>
    <li><a href="../steffen.html">Steffen</a></li>
    <li><a href="../robin.html" >Robin</a></li>
    <li><a href="#contact" class="scroll">Contact Us</a></li>
  </ul>
</div><!-- /.navbar-collapse -->	

</nav>
<div class="logo">
  <a href="../index.html">VLQ<span>WbXWbx</span></a>
</div>
<div class="dummy_text">
  <p>motivational speech</p>
</div>

</div>
</div>
<!-- //banner -->
        """)
        f.close()


def printPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""<!-- portfolio -->
<div class="portfolio">
<div class="container">
  <h3>"""+studyName+"""</h3>
<div class="portfolio-grids">
<div class="sap_tabs">			
<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
	  <ul class="resp-tabs-list">
        """)
        f.close()

def printNav(pages):
    with open(studyName+'.html', "a") as f:
        f.write("""<li class="resp-tab-item"><span>"""+pages+"""</span></li>
        """)
        f.close()

def EndPrintNav():
    with open(studyName+'.html', "a") as f:
        f.write("""</ul>

        """)
        f.close()
        
def preparePlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="resp-tabs-container">
	<div class="tab-1 resp-tab-content">								
	<div class="main">
        """)
        f.close()


def printPlot(subdir, plot):
    d_plot = os.path.splitext(plot)
    with open(studyName+'.html', "a") as f:
        f.write("""<div class="view">
	<div class="img-top">
	  <a href='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' rel="Download as <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".pdf'>.pdf</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".png'>.png</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".root'>.root</a>" class="b-link-stripe b-animate-go  thickbox">
	  <img src='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' class="img-responsive" alt="" />
	<div class="mask"></div>
	<div class="content">
	  <span class="info" title="extra information if needed" </span>
	</div>
	</a>
	</div>
	</div>
        """)
        f.close()

def EndPlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="clearfix"> </div>
	</div>																					 	        					 
	</div>
    </div>
        """)
        f.close()

        

def PrintEndPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""	
		</div>
	</div>
	</div>
    	<script src="../js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion           
	width: 'auto', //auto or any width like 600px
	fit: true   // 100% fit in a container
	});
	});
	
	</script>
	</div>
	</div>
	</div>
<!-- //portfolio -->
        """)
        f.close()


def PrintContact():
    with open(studyName+'.html', "a") as f:
        f.write("""	
<!-- contact -->
	<div class="contact" id="contact">
	  
	<div class="container">
	  <h3>Contact Me</h3>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
	  <h4>Address</h4>
	  <p>6224 Agricultural
	  Road<span>Vancouver, Canada</span></p>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
	  <h4>Mail</h4>
	  <a href='mailto:"""+email+"""'>"""+email+"""</a>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
	  <h4>Phone</h4>
	  <p>...</p>
	</div>
	<div class="clearfix"> </div>
	<!-- footer -->
	<div class="footer-copy">
	  <p>&copy 2016 Your Trip. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts.</a></p>
	</div>
	</div>
	</div>
	<!-- //contact -->
        """)
        f.close()

def EndFile():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
	$(document).ready(function() {
	/*
	var defaults = {
	containerID: 'toTop', // fading element id
	containerHoverID: 'toTopHover', // fading element hover id
	scrollSpeed: 1200,
	easingType: 'linear' 
	};
	*/
	
	$().UItoTop({ easingType: 'easeOutQuart' });
	
	});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	</body>
	</html>
        """)
        f.close()

        
def makeHTML_init():
    printHeader()
    printBanner()
    printPortfolio()


def makeHTML(subdir, allPlots):
    preparePlotPage()
    for plot in allPlots:
        plot = plot.lstrip('./')
        printPlot(subdir, plot)
        #FillIndex_organisePlotsInTable(index,plot,nPlots)
#    FillIndex_organisePlotsInTableEnd(index)
    EndPlotPage()



def makeHTML_finalize():
    PrintEndPortfolio()
    PrintContact()
    EndFile()

def main():
    loc = os.getcwd()
    makeHTML_init()
    allSubDirs = os.walk('.').next()[1]
    for subdir in allSubDirs:
        printNav(subdir)
    EndPrintNav()
    for subdir in allSubDirs:
        os.chdir(subdir)
        nPlots = os.walk('.').next()[2]
        print nPlots
        allPNG = glob.glob("./*.png")
        print 'In subdir : '+str(subdir)+' there are '+str(nPlots)+' namely : '+str(allPNG)
        os.chdir(loc)
        makeHTML(subdir, allPNG)
    makeHTML_finalize()
#    for png in allPNG:
    #    print png



main()
