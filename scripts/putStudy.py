#!/env/python
import glob,os,sys,re,codecs
from html import HTML
from sys import version_info
from HTMLParser import HTMLParser

#SETUP
host="http://atlas-t3-ubc.westgrid.ca/UBC/"
analysisFolder = "sthenkel/plotlog"

mainUser = "performance" #due to indexpage handling


piece = raw_input("What do you want to upload? (pres, plots) >> : ").lower()
pieceList = set(['Pres','pres','plots',
                'Plots'])
    
if piece not in pieceList:
    print "Piece is not in piece list, sorry"
    print "INFO :: Exiting ..."
    sys.exit()

if "pres" in piece:
    allPDFs = glob.glob("./*.pdf")
    Pres = raw_input('INFO :: Which pdfs do you want to upload? (needs to be in current directory)\n '+str(allPDFs)+' Please copy & paste the presentation you want to upload out of the list in format ./<presentation>.pdf >> : ')

    
if "plots" in piece:
    response = raw_input("INFO :: Name your study (one string) [MUST] >> : ")

    if response is "":
        print "You provided no name for your study"
        print "INFO :: Exiting ..."
        sys.exit()
    else:
        studyName = re.sub(r'[^A-Za-z0-9]+', "", response)
else:
    studyName = "presentation"
    
location = raw_input("Are you on the flashy cluster? : ").lower()
yes = set(['yes','y', 'ye', 'YES','Yes','j','Ja','ja'])
no = set(['no','n','NO','No'])
if location in yes:
    inNetwork = True
    hostlocation = "/data/www/"
elif location in no:
    inNetwork = False
    hostlocation = "sthenkel@atlas-t3-ubc.westgrid.ca:/data/www/"
else:
    print "Please respond with 'yes' or 'no'"
    print "INFO :: Exiting ..."
    sys.exit()


    
user = raw_input("Which group? (performance, analyses, testarea)) [MUST] (lower case) >> : ").lower()
    
userList = set(['Performance','Analyses',
                'performance','analyses',
                'testarea','Testarea','TestArea'])
    
if user not in userList:
    print "User is not in user list, sorry"
    print "INFO :: Exiting ..."
    sys.exit()

if "performance" in user:
    project = raw_input("Which project? (alignment, tcp)) [MUST] (lower case)>> : ").lower()
if "analyses" in user:
    project = raw_input("Which project? (diffxsec, vlqwbx)) [MUST] (lower case)>> : ").lower()
projectList = set(['diffxsec','vlqwbx',
                'alignment','tcp','testarea'])
if "testarea" in user:
    project = "testarea"
    print "INFO :: plots will be put into the testarea"
    
if project not in projectList:
    print "User is not in user list, sorry"
    print "INFO :: Exiting ..."
    sys.exit()

    
choice = raw_input("Do you want to update your navigation (link to your study)? [MUST] [y/n] >> : ").lower()
yes = set(['yes','y', 'ye', 'YES','Yes','yep','Yep','j','Ja','ja'])
no = set(['no','n','NO','No','Nope','nope'])
if choice in yes:
   NavUpdate = True
elif choice in no:
   NavUpdate = False
else:
    print "Please respond with 'yes' or 'no'"
    print "INFO :: Exiting ..."
    sys.exit()



email = "steffen.henkelmann@cern.ch"
    
analysis = ['PlotLog','an automated plot viewing tool (alpha)']

h = HTML()

def printHeader():
    study = open(studyName + '.html', 'w')
    print >> study.write("""<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>plotlog | Portfolio :: w3layouts</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Your Trip Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="../js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<!-- pop-up-script -->
<script src="../js/jquery.chocolat.js"></script>
		<link rel="stylesheet" href="../css/chocolat.css" type="text/css" media="screen" charset="utf-8">
		<!--light-box-files -->
		<script type="text/javascript" charset="utf-8">
		$(function() {
			$('.img-top a').Chocolat();
		});
		</script>
<!-- //pop-up-script -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="../js/move-top.js"></script>
<script type="text/javascript" src="../js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
</head>
    """)
    study.close()


def printBannerBeg():
    with open(studyName+'.html', "a") as f:
        f.write("""<body>
<!-- banner -->
<div class="banner">
<div class="container">
<div class="banner_top">
<div class="banner_top_left">
  <p>steffen.henkelmann @UBC</p>
</div>
<div class="banner_top_right">
  <form>
  <input type="text" value="Search Here..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Address';}" required="">
  <i class="glyphicon glyphicon-search" aria-hidden="true"></i>
  </form>
</div>
<div class="clearfix"> </div>
</div>
<nav class="navbar navbar-default">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
  <span class="sr-only">Toggle navigation</span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  <span class="icon-bar"></span>
  </button>
</div>
        """)
        f.close()

def printBannerEnd():
    with open(studyName+'.html', "a") as f:
        f.write("""
        </nav>
<div class="logo">
  <a href="https://gitlab.cern.ch/sthenkel/personalPlotLog">"""+analysis[0]+"""<span>"""+analysis[1]+"""</span></a>
</div>
<div class="dummy_text">
  <p></p>
</div>

</div>
</div>
<!-- //banner -->
""")
        f.close()
def printPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""<!-- portfolio -->
<div class="portfolio">
<div class="container">
  <h3>"""+studyName+"""</h3>
<div class="portfolio-grids">
<div class="sap_tabs">			
<div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
	  <ul class="resp-tabs-list">
        """)
        f.close()

def printNav(pages):
    with open(studyName+'.html', "a") as f:
        f.write("""<li class="resp-tab-item"><span>"""+pages+"""</span></li>
        """)
        f.close()

def EndPrintNav():
    with open(studyName+'.html', "a") as f:
        f.write("""</ul>

        """)
        f.close()
        
def preparePlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="resp-tabs-container">
	<div class="tab-1 resp-tab-content">								
	<div class="main">
        """)
        f.close()


def printPlot(subdir, plot):
    d_plot = os.path.splitext(plot)
    with open(studyName+'.html', "a") as f:
        f.write("""<div class="view">
	<div class="img-top">
	  <a href='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' rel="Download as <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".pdf'>.pdf</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".png'>.png</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".root'>.root</a> | <a href='"""+studyName+"""/"""+subdir+"""/"""+d_plot[0]+""".dat'>.dat</a>" class="b-link-stripe b-animate-go  thickbox">
	  <img src='"""+studyName+"""/"""+subdir+"""/"""+plot+"""' class="img-responsive" alt="" />
	<div class="mask"></div>
	<div class="content">
	  <span class="info" title="extra information if needed" </span>
	</div>
	</a>
	</div>
	</div>
        """)
        f.close()

def EndPlotPage():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<div class="clearfix"> </div>
	</div>																					 	        					 
	</div>
    </div>
        """)
        f.close()

        

def PrintEndPortfolio():
    with open(studyName+'.html', "a") as f:
        f.write("""	
		</div>
	</div>
	</div>
    	<script src="../js/easyResponsiveTabs.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function () {
	$('#horizontalTab').easyResponsiveTabs({
	type: 'default', //Types: default, vertical, accordion           
	width: 'auto', //auto or any width like 600px
	fit: true   // 100% fit in a container
	});
	});
	
	</script>
	</div>
	</div>
	</div>
<!-- //portfolio -->
        """)
        f.close()


def PrintContact():
    with open(studyName+'.html', "a") as f:
        f.write("""	
<!-- contact -->
	<div class="contact" id="contact">
	  
	<div class="container">
	  <h3>Contact Me</h3>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
	  <h4>Address</h4>
	  <p>6224 Agricultural
	  Road<span>Vancouver, Canada</span></p>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
	  <h4>Mail</h4>
	  <a href='mailto:"""+email+"""'>"""+email+"""</a>
	</div>
	<div class="col-md-4 contact-grid">
	  <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
	  <h4>Phone</h4>
	  <p>...</p>
	</div>
	<div class="clearfix"> </div>
	<!-- footer -->
	<div class="footer-copy">
	  <p>&copy 2016 Your Trip. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts.</a></p>
	</div>
	</div>
	</div>
	<!-- //contact -->
        """)
        f.close()

def EndFile():
    with open(studyName+'.html', "a") as f:
        f.write("""	
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
	$(document).ready(function() {
	/*
	var defaults = {
	containerID: 'toTop', // fading element id
	containerHoverID: 'toTopHover', // fading element hover id
	scrollSpeed: 1200,
	easingType: 'linear' 
	};
	*/
	
	$().UItoTop({ easingType: 'easeOutQuart' });
	
	});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->
	</body>
	</html>
        """)
        f.close()
def printBannerNav(user):
    with open(studyName+'.html', "a") as f:
        if "performance" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html"class="active">Performance</a></li>
            <li><a href="../analyses.html">Analyses</a></li>
            <li><a href="../testarea.html">TestArea</a></li>
            <li><a href="#contact" class="scroll">Contact Me</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
        elif "analyses" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Performance</a></li>
            <li><a href="../analyses.html"class="active">Analyses</a></li>
            <li><a href="../testarea.html">TestArea</a></li>
            <li><a href="#contact" class="scroll">Contact Me</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
        elif "testarea" in user:
            f.write("""
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Performance</a></li>
            <li><a href="../analyses.html">Analyses</a></li>
            <li><a href="../testarea.html"class="active">TestArea</a></li>
            <li><a href="#contact" class="scroll">Contact Me</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()
            
        else:
            f.write("""
           <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav cl-effect-14">
            <li><a href="../index.html">Performance</a></li>
            <li><a href="../analyses.html">Analyses</a></li>
            <li><a href="../testarea.html">TestArea</a></li>
            <li><a href="#contact" class="scroll">Contact Me</a></li>
            </ul>
            </div><!-- /.navbar-collapse -->	
            """)
            f.close()

        
def makeHTML_init():
    printHeader()
    printBannerBeg()
    printBannerNav(user)
    printBannerEnd()
    printPortfolio()


def makeHTML(subdir, allPlots):
    preparePlotPage()
    for plot in allPlots:
        print plot
        plot = plot.lstrip('./')
        printPlot(subdir, plot)
        #FillIndex_organisePlotsInTable(index,plot,nPlots)
#    FillIndex_organisePlotsInTableEnd(index)
    EndPlotPage()



def makeHTML_finalize():
    PrintEndPortfolio()
    PrintContact()
    EndFile()

def produceHTML():
    loc = os.getcwd()
    makeHTML_init()
    allSubDirs = os.walk('.').next()[1]
    for subdir in allSubDirs:
        if str(studyName) in str(subdir):
            continue
        else:
            printNav(subdir)
            
    EndPrintNav()
    for subdir in allSubDirs:
        os.chdir(subdir)
        nPlots = os.walk('.').next()[2]
#        print nPlots
        allPNG = glob.glob("./*.png")
#        print 'In subdir : '+str(subdir)+' there are '+str(nPlots)+' namely : '+str(allPNG)
        os.chdir(loc)
        if not str(studyName) in str(subdir):
            os.system("cp -r " + subdir + " " + studyName)
        makeHTML(subdir, allPNG)
    makeHTML_finalize()
#    for png in allPNG:
    #    print png


def uploadPlots():
    print "INFO :: Will now upload the files to the group webpage for you, " + user
    if inNetwork:
        os.system("cp " + studyName + ".html " + hostlocation + "/" + analysisFolder + "/" + user + "/")
        os.system("cp -r " + studyName + " " +hostlocation + "/"+ analysisFolder + "/" + user + "/")       
    else:
        print "You are not on flashy so will copy via scp ..."
        os.system("scp " + studyName + ".html " + hostlocation + "/" + analysisFolder + "/" + user + "/")
        os.system("scp -r " + studyName + " " + hostlocation + "/"+ analysisFolder + "/" + user + "/")

def uploadPres():
    print "INFO :: Will now upload the files to the group webpage for you, " + user
    if inNetwork:
        os.system("cp " + Pres + " " + hostlocation + "/" + analysisFolder + "/" + user + "/pres")
    else:
        print "You are not on flashy so will copy via scp ..."
        os.system("scp " + Pres + " " + hostlocation + "/" + analysisFolder + "/" + user + "/pres")

                
def changeNavigation(user):
    print "INFO :: Will update your navigation and link to the new study"
    respTitle = raw_input("Please provide a name of your study (e.g. ttbar control plots) >> : ")
    if respTitle is "":
        print "You provided no name for your study, so the computer chose one"
        NameOfStudy = "I was too lazy to enter a study name"
    else:
        NameOfStudy = respTitle
        
    respContext = raw_input("Please provide a minimal description (e.g. which AT version used, selection) >> : ")
    if respContext is "":
        print "You provided no name for your study, so the computer chose one"
        context = "I was too lazy to provide study context"
    else:
        context = respContext

    print '###########################################'
    print "# Name of your study: " + NameOfStudy
    print '--------------------------------------------------'
    print "# Context : "
    print "# " + context
    print '###########################################'    


    if "plots" in piece:
        if "alignment" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-ALIGNMENT-PLOTS-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/"""+studyName+""".html'>more</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-ALIGNMENT-PLOTS-->"""
        if "tcp" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-TRACKINGCP-PLOTS-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
        <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/"""+studyName+""".html'>more</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-TRACKINGCP-PLOTS-->"""
        if "diffxsec" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-DIFFXSEC-PLOTS-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/"""+studyName+""".html'>more</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-DIFFXSEC-PLOTS-->"""
        if "vlqwbx" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-VLQWBX-PLOTS-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/"""+studyName+""".html'>more</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-VLQWBX-PLOTS-->"""
    
        if "testarea" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-TESTAREA-PLOTS-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-th" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/"""+studyName+""".html'>more</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-TESTAREA-PLOTS-->"""











            
    if "pres" in piece:
        if "alignment" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-ALIGNMENT-PRES-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/pres/"""+Pres+"""'>here</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-ALIGNMENT-PRES-->"""
        if "tcp" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-TRACKINGCP-PRES-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
        <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/pres/"""+Pres+"""'>here</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-TRACKINGCP-PRES-->"""
        if "diffxsec" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-DIFFXSEC-PRES-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/pres/"""+Pres+"""'>here</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-DIFFXSEC-PRES-->"""
        if "vlqwbx" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-VLQWBX-PRES-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/pres/"""+Pres+"""'>here</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-VLQWBX-PRES-->"""
    
        if "testarea" in project:
            stringToReplace = r"""<div class="clearfix"> </div><!--ForExternalPython-TESTAREA-PRES-->"""
            replacement = r"""<div class="col-md-3 featured-services-grid">
<div class="featured-services-grd">
<span class="glyphicon glyphicon-file" aria-hidden="true"></span>
<h4>"""+NameOfStudy+"""</h4>
    <p>"""+context+"""</p>
<div class="more m2">
<a class="btn effect6" href='"""+user+"""/pres/"""+Pres+"""'>here</a>
    </div>
    </div>
    </div>
    <div class="clearfix"> </div><!--ForExternalPython-TESTAREA-PRES-->"""

    if mainUser in str(user):
        USER = "index"
    else:
        USER = user

    if inNetwork:
        print hostlocation + "/" + analysisFolder + "/" + USER + '.html'
        with codecs.open(hostlocation + "/" + analysisFolder + "/" + USER + '.html','r',encoding='utf8') as f:
            f_html = f.read()
            new_html = re.sub(stringToReplace, replacement, f_html)
            f.close()
        with codecs.open(hostlocation + "/" + analysisFolder + "/" + USER + '.html','w',encoding='utf8') as nf:
            nf.write(new_html)
            nf.close()

    else:
        os.system("scp " + hostlocation + "/" + analysisFolder + "/" + USER + '.html .')
        with codecs.open(USER + '.html','r',encoding='utf8') as f:
            f_html = f.read()
            new_html = re.sub(stringToReplace, replacement, f_html)
            f.close()
        with codecs.open(USER + '.html','w',encoding='utf8') as nf:
            nf.write(new_html)
            nf.close()
            os.system("scp " + USER + '.html ' + hostlocation + "/" + analysisFolder + "/." )
            os.system("rm -f " + USER + '.html')

        
if __name__ == "__main__":

    if "pres" not in piece:
        os.system("mkdir -p " + studyName)
        produceHTML()
        uploadPlots()
    else:
        uploadPres()
    print 'INFO :: Will upload your study'

    if NavUpdate:
      print 'INFO :: Will update the navigation and add a study to your page ...'
      changeNavigation(user)
    else:
      print 'INFO :: Will only provide and print the URL under which you can find the plots,'
      print '        No link from your main navigation will be provided ...'
      print "         >>> " + host + analysisFolder + "/" + user + "/" + studyName + ".html"
      
